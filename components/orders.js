const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017';
const dbName = 'ecommerce';
const client = new MongoClient(url);

module.exports = {
  create: async function (dataToInsert) {
    await this.errorHandling(async (orders) => {
      if (Array.isArray(dataToInsert)) {
        await orders.insertMany(dataToInsert);
      } else {
        await orders.insertOne(dataToInsert);
      }
      console.log('Success');
    });
  },
  read: async function (query) {
    await this.errorHandling(async (orders) => {
      const res = await orders.find(query).toArray();
      console.log('Success');
      console.log(res);
    });
  },
  update: async function (fromData, toData) {
    await this.errorHandling(async (orders) => {
      await orders.updateMany(fromData, { $set: toData });
      console.log('Success');
    });
  },
  delete: async function (dataToDelete) {
    await this.errorHandling(async (orders) => {
      await orders.deleteMany(dataToDelete);
      console.log('Success');
    });
  },
  errorHandling: async function (execFunc) {
    try {
      await client.connect();
      const database = client.db(dbName);
      const orders = database.collection('orders');
      await execFunc(orders);
    } catch (e) {
      console.log('Error');
      console.log(e);
    } finally {
      console.log('Closing Connection');
      await client.close();
    }
  },
};