const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017';
const dbName = 'ecommerce';
const client = new MongoClient(url);

module.exports = {
  create: async function (dataToInsert) {
    await this.errorHandling(async (roles) => {
      if (Array.isArray(dataToInsert)) {
        await roles.insertMany(dataToInsert);
      } else {
        await roles.insertOne(dataToInsert);
      }
      console.log('Success');
    });
  },
  read: async function (query) {
    await this.errorHandling(async (roles) => {
      const res = await roles.find(query).toArray();
      console.log('Success');
      console.log(res);
    });
  },
  update: async function (fromData, toData) {
    await this.errorHandling(async (roles) => {
      await roles.updateMany(fromData, { $set: toData });
      console.log('Success');
    });
  },
  delete: async function (dataToDelete) {
    await this.errorHandling(async (roles) => {
      await roles.deleteMany(dataToDelete);
      console.log('Deleted');
    });
  },
  errorHandling: async function (execFunc) {
    try {
      await client.connect();
      const database = client.db(dbName);
      const roles = database.collection('roles');
      await execFunc(roles);
    } catch (e) {
      console.log('Error');
      console.log(e);
    } finally {
      console.log('Closing Connection');
      await client.close();
    }
  },
};