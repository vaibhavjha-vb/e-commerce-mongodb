const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017';
const dbName = 'ecommerce';
const client = new MongoClient(url);

module.exports = {
  create: async function (dataToInsert) {
    await this.errorHandling(async (products) => {
      if (Array.isArray(dataToInsert)) {
        await products.insertMany(dataToInsert);
      } else {
        await products.insertOne(dataToInsert);
      }
      console.log('Success');
    });
  },
  read: async function (query) {
    await this.errorHandling(async (products) => {
      const res = await products.find(query).toArray();
      console.log('Success');
      console.log(res);
    });
  },
  update: async function (fromData, toData) {
    await this.errorHandling(async (products) => {
      await products.updateMany(fromData, { $set: toData });
      console.log('Success');
    });
  },
  delete: async function (dataToDelete) {
    await this.errorHandling(async (products) => {
      await products.deleteMany(dataToDelete);
      console.log('Deleted');
    });
  },
  errorHandling: async function (execFunc) {
    try {
      await client.connect();
      const database = client.db(dbName);
      const products = database.collection('products');
      await execFunc(products);
    } catch (e) {
      console.log('Error');
      console.log(e);
    } finally {
      console.log('Closing Connection');
      await client.close();
    }
  },
};