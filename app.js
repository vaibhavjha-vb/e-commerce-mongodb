const {
    roles,
    tags,
    users,
    categories,
    products,
    carts,
    orders,
  } = require('./components');
  const ecomData = require('./data.json');
  (async () => {

    await users.create(ecomData.users);
    await users.update({ firstname: 'Gopal' }, { firstname: 'Hari' });
    await users.read({});
    await users.delete({ firstname: 'Hari' });
 
    
    await roles.create(ecomData.roles);
    await roles.update({ name: 'Client' }, { name: 'Customer' });
    await roles.delete({ name: 'Buyer' });
    await roles.read({});


    await tags.create(ecomData.tags);
    await tags.update({ slug: 'CLR' }, { slug: 'CLRC' });
    await tags.delete({ slug: 'DIS' });
    await tags.read({});

    
    await categories.create(ecomData.categories);
    await categories.update({ slug: 'drinks' }, { description: 'modified' });
    await categories.read({});
    await categories.delete({ slug: 'drinks' });


    await products.create(ecomData.products);
    await products.update({ name: 'Coca-Cola' }, { tags: ['modified'] });
    await products.read({});
    await products.delete({ name: 'Coca-Cola' });


   
    await orders.create(ecomData.orders);
    await orders.update({ user_id: '45GH78' }, { products: ['modified'] });
    await orders.read({});
    await orders.delete({ user_id: '45GH78' });
    

    await carts.create(ecomData.carts);
    await carts.update({ user: 'Sam Sharon' }, { products: ['modified'] });
    await carts.read({});
    await carts.delete({ user: 'Sam Sharon' });
  })().catch(console.dir);